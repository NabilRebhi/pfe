// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:iwaybot/postgres_utils/postgres_services.dart';
import 'package:iwaybot/views/add_module.dart';
import 'package:url_launcher/url_launcher.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  late List<Widget> list = [];
  late Timer _refreshTimer;
  late int moduleCount = 0;

  late String getBuildsByModuleResult = "none";
  late String getBuildsResult = "none";
  late String getCommitsResult = "none";
  late String role = "none";

  Future<void> _refresh() async {
    late String getModulesResult = "none";
    await PostgresServices.getModules().then((value) {
      getModulesResult = value;
    });
    if (getModulesResult == "done") {
      setState(() {
        moduleCount = PostgresServices.modules.getRow()!.length;
      });

      await PostgresServices.getBuilds().then((value) {
        setState(() {
          getBuildsResult = value;
        });
      });

      buildWidgetsList(context, moduleCount);
    }
  }

  Widget row1(context, String value, String host, String port) {
    late Widget r = const SizedBox();
    final Uri url = Uri.parse("http://$host:$port");
    if (value == "testing") {
      r = const Text("creating instance...");
    } else if (value == "done") {
      r = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("Visit:  "),
          TextButton(
              onPressed: () async {
                if (await canLaunchUrl(url)) {
                  await launchUrl(url);
                } else {
                  return alert(
                    context,
                    title: const Text("Something went wrong!"),
                    content: const Text("Could not launch"),
                    textOK: const Text("GOT IT"),
                  );
                }
              },
              child: const Text("->HERE<-"))
        ],
      );
    } else {
      r = const Text("ERROR!!");
    }
    return r;
  }

  Widget row2(context, String value, String containerName) {
    late Widget r = const SizedBox();
    if (value == "done") {
      r = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("Logs:  "),
          TextButton(
              onPressed: () async {
                await PostgresServices.getLogs(containerName: containerName)
                    .then((value) {
                  return alert(
                    context,
                    title: Text("$containerName Logs"),
                    content: SingleChildScrollView(child: Text(value)),
                    textOK: const Text("GOT IT"),
                  );
                });
              },
              child: const Text("->HERE<-"))
        ],
      );
    }
    return r;
  }

  Widget row3(context, String value, List? module, List? build) {
    late Widget r = const SizedBox();
    if (value == "done") {
      r = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("Success?: "),
          TextButton(
              onPressed: () {
                //function that triggers the next pipeline in Jenkins
                PostgresServices.testCompleted(
                    moduleData: module, buildData: build);
              },
              child: const Text("->Yes<-"))
        ],
      );
    }
    return r;
  }

  void buildWidgetsList(context, int data) async {
    late List<Widget> l = [];
    if (getBuildsResult == "done") {
      for (int i = 0; i < data; i++) {
        await PostgresServices.getBuildsByModule(
            moduleId:
                PostgresServices.modules.getRow()!.elementAt(i).elementAt(0));

        l.add(Row(children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.25,
            width: MediaQuery.of(context).size.width * 0.25,
            child: Padding(
              padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.008,
                MediaQuery.of(context).size.height * 0.008,
                MediaQuery.of(context).size.width * 0.008,
                MediaQuery.of(context).size.height * 0.008,
              ),
              child: DecoratedBox(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("Module Name:"),
                    Text(
                        "${PostgresServices.modules.getRow()!.elementAt(i).elementAt(7)}"),
                    const Text("Branch:"),
                    Text(
                        "${PostgresServices.modules.getRow()!.elementAt(i).elementAt(1)}"),
                  ],
                ),
              ),
            ),
          ),
          for (int j = 0;
              j < PostgresServices.modules.getRow()!.elementAt(i).elementAt(5);
              j++)
            (SizedBox(
              height: MediaQuery.of(context).size.height * 0.25,
              width: MediaQuery.of(context).size.width * 0.25,
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.008,
                  MediaQuery.of(context).size.height * 0.008,
                  MediaQuery.of(context).size.width * 0.008,
                  MediaQuery.of(context).size.height * 0.008,
                ),
                child: DecoratedBox(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Commit ID: ${PostgresServices.buildsByModule.getRow()!.elementAt(j).elementAt(0)}",
                        style: TextStyle(
                            color: buildColor(PostgresServices.buildsByModule
                                .getRow()!
                                .elementAt(j)
                                .elementAt(2))),
                      ),
                      row1(
                          context,
                          PostgresServices.buildsByModule
                              .getRow()!
                              .elementAt(j)
                              .elementAt(2),
                          PostgresServices.buildsByModule
                              .getRow()!
                              .elementAt(j)
                              .elementAt(4),
                          PostgresServices.buildsByModule
                              .getRow()!
                              .elementAt(j)
                              .elementAt(5)),
                      row2(
                          context,
                          PostgresServices.buildsByModule
                              .getRow()!
                              .elementAt(j)
                              .elementAt(2),
                          "${PostgresServices.buildsByModule.getRow()!.elementAt(j).elementAt(0)}_odoo"),
                      row3(
                          context,
                          PostgresServices.buildsByModule
                              .getRow()!
                              .elementAt(j)
                              .elementAt(2),
                          PostgresServices.modules.getRow()!.elementAt(i),
                          PostgresServices.buildsByModule
                              .getRow()!
                              .elementAt(j))
                    ],
                  ),
                ),
              ),
            ))
        ]));
      }
    }
    setState(() {
      list = l;
    });
  }

  Color buildColor(String value) {
    if (value == "done") {
      return Colors.green;
    } else if (value == "error") {
      return Colors.red;
    } else {
      return Colors.black;
    }
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      role = PostgresServices.agent.getRow()!.elementAt(0).elementAt(2);
    });

    _refreshTimer = Timer.periodic(const Duration(minutes: 1), (timer) {
      _refresh();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _refreshTimer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Dashboard"),
        actions: [
          if (role == "developer")
            (IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const AddModule()),
                  );
                },
                icon: const Icon(Icons.add)))
        ],
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: const Color.fromARGB(103, 0, 0, 0),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: list,
          ),
        ),
      ),
    );
  }
}
