// ignore_for_file: use_build_context_synchronously

import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:iwaybot/postgres_utils/postgres_services.dart';

import 'package:iwaybot/views/dashboard.dart';

class AddModule extends StatefulWidget {
  const AddModule({super.key});

  @override
  State<AddModule> createState() => _AddModuleState();
}

class _AddModuleState extends State<AddModule> {
  final TextEditingController _repoUsername = TextEditingController();
  final TextEditingController _repoName = TextEditingController();
  final TextEditingController _token = TextEditingController();
  final TextEditingController _branch = TextEditingController();
  final TextEditingController _version = TextEditingController();
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  late bool check;
  late String result = "none";

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add module"),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Dashboard()),
                );
              },
              icon: const Icon(Icons.dashboard)),
        ],
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: const Color.fromARGB(103, 0, 0, 0),
        child: Center(
          child: SizedBox(
            height: MediaQuery.of(context).size.height * 1,
            width: MediaQuery.of(context).size.width * 0.7,
            child: Padding(
              padding: EdgeInsets.fromLTRB(
                0,
                MediaQuery.of(context).size.height * 0.008,
                MediaQuery.of(context).size.width * 0.008,
                MediaQuery.of(context).size.height * 0.008,
              ),
              child: DecoratedBox(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: SingleChildScrollView(
                  child: Form(
                    key: _form,
                    child: Column(
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              "GitLab username: ",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width * 0.3,
                              ),
                              child: TextFormField(
                                controller: _repoUsername,
                                validator: (validator) {
                                  if (validator!.isEmpty) {
                                    return 'Empty !';
                                  }

                                  return null;
                                },
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  hintText: "Username",
                                  filled: true,
                                  hintStyle: TextStyle(color: Colors.grey[800]),
                                  fillColor: Colors.white70,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              "GitLab repository name: ",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width * 0.3,
                              ),
                              child: TextFormField(
                                controller: _repoName,
                                validator: (validator) {
                                  if (validator!.isEmpty) {
                                    return 'Empty !';
                                  }

                                  return null;
                                },
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  hintText: "Repository Name",
                                  filled: true,
                                  hintStyle: TextStyle(color: Colors.grey[800]),
                                  fillColor: Colors.white70,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              "Branch: ",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width * 0.3,
                              ),
                              child: TextFormField(
                                controller: _branch,
                                validator: (validator) {
                                  if (validator!.isEmpty) {
                                    return 'Empty !';
                                  }

                                  return null;
                                },
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  hintText: "Exemple: module-test",
                                  filled: true,
                                  hintStyle: TextStyle(color: Colors.grey[800]),
                                  fillColor: Colors.white70,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              "GitLab Token: ",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width * 0.3,
                              ),
                              child: TextFormField(
                                controller: _token,
                                validator: (validator) {
                                  if (validator!.isEmpty) {
                                    return 'Empty !';
                                  }

                                  return null;
                                },
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  filled: true,
                                  hintText: "Token",
                                  hintStyle: TextStyle(color: Colors.grey[800]),
                                  fillColor: Colors.white70,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              "Odoo version: ",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width * 0.3,
                              ),
                              child: TextFormField(
                                controller: _version,
                                validator: (validator) {
                                  if (validator!.isEmpty) {
                                    return 'Empty !';
                                  }

                                  return null;
                                },
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  hintText: "Exemple: 13",
                                  filled: true,
                                  hintStyle: TextStyle(color: Colors.grey[800]),
                                  fillColor: Colors.white70,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                        ),
                        DecoratedBox(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.green,
                          ),
                          child: TextButton(
                            onPressed: () async {
                              setState(() {
                                check = _form.currentState!.validate();
                              });
                              if (check == true) {
                                await PostgresServices.createModule(
                                        username: _repoUsername.text,
                                        repoName: _repoName.text,
                                        branch: _branch.text,
                                        token: _token.text,
                                        version: _version.text)
                                    .then((value) {
                                  setState(() {
                                    result = value;
                                  });
                                });
                                if (result == "done") {
                                  return alert(
                                    context,
                                    title: const Text(
                                        "Module saved successfully!"),
                                    content: Text(result),
                                    textOK: const Text("GOT IT"),
                                  );
                                } else {
                                  return alert(
                                    context,
                                    title:
                                        const Text("Something went wrong !!"),
                                    content: Text(result),
                                    textOK: const Text("GOT IT"),
                                  );
                                }
                              }
                            },
                            child: SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                              width: MediaQuery.of(context).size.width * 0.25,
                              child: const Center(
                                child: Text(
                                  "Add Module",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
