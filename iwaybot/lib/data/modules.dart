class ModulesData {
  ModulesData();

  List? _rowForModules;

  List? getRow() {
    return _rowForModules;
  }

  setRow(List? rowForModules) {
    _rowForModules = rowForModules;
  }
}
