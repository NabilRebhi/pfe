class AgentData {
  AgentData();

  List? _rowForAgent;

  List? getRow() {
    return _rowForAgent;
  }

  setRow(List? rowForAgent) {
    _rowForAgent = rowForAgent;
  }
}
