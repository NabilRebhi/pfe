import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:iwaybot/data/agent.dart';
import 'package:iwaybot/data/builds.dart';
import 'package:iwaybot/data/builds_by_module.dart';
import 'package:iwaybot/data/modules.dart';

class PostgresServices {
  PostgresServices();

  static ModulesData modules = ModulesData();
  static BuildsData builds = BuildsData();
  static BuildsByModuleData buildsByModule = BuildsByModuleData();
  static AgentData agent = AgentData();
  static String apiHost = "http://192.168.8.122";
  static String uri = "";
/*
-------------------------------------
             login
-------------------------------------
*/
  static Future<String> login(
      {required String username, required String password}) async {
    late String result = "none";
    final headers = {'Content-Type': 'application/json'};
    final body = jsonEncode({'username': username, 'password': password});
    try {
      final response = await http.post(Uri.parse("$apiHost/api/login"),
          headers: headers, body: body);
      final data = jsonDecode(response.body);
      if (data['message'] == "User logged in successfully") {
        result = data['message'];
        agent.setRow(data['data']);
      } else if (data['message'] == "Invalid username or password") {
        result = data['message'];
      } else {
        result == "Something went wrong";
      }
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

/*
-------------------------------------
             get modules
-------------------------------------
*/
  static Future<String> createModule({
    required String username,
    required String repoName,
    required String branch,
    required String token,
    required String version,
  }) async {
    late String result = "none";
    final String moduleId = "$username-$repoName";
    final headers = {'Content-Type': 'application/json'};
    final body = jsonEncode({
      'module_id': moduleId,
      'branch': branch,
      'token': token,
      'odoo_version': version,
      'username': username,
      'repo_name': repoName
    });
    try {
      final response = await http.post(Uri.parse("$apiHost/api/module/create"),
          headers: headers, body: body);
      final data = jsonDecode(response.body);
      if (data['message'] == "success") {
        result = "done";
        modules.setRow(data['data']);
      } else {
        result == "Something went wrong";
      }
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

/*
-------------------------------------
             get modules
-------------------------------------
*/
  static Future<String> getModules() async {
    late String result = "none";
    final headers = {'Content-Type': 'application/json'};
    try {
      final response = await http.post(Uri.parse("$apiHost/api/module/getall"),
          headers: headers);
      final data = jsonDecode(response.body);
      if (data['message'] == "success") {
        result = "done";
        modules.setRow(data['data']);
      } else {
        result == "Something went wrong";
      }
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

  /*
-------------------------------------
             get builds
-------------------------------------
*/
  static Future<String> getBuilds() async {
    late String result = "none";
    final headers = {'Content-Type': 'application/json'};
    try {
      final response = await http.post(Uri.parse("$apiHost/api/build/getall"),
          headers: headers);
      final data = jsonDecode(response.body);
      if (data['message'] == "success") {
        result = "done";
        builds.setRow(data['data']);
      } else {
        result == "Something went wrong";
      }
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

  /*
-------------------------------------
        get builds by module
-------------------------------------
*/
  static Future<String> getBuildsByModule({required String moduleId}) async {
    late String result = "none";
    final headers = {'Content-Type': 'application/json'};
    final body = jsonEncode({'module_id': moduleId});
    try {
      final response = await http.post(
          Uri.parse("$apiHost/api/build/getbymodule"),
          headers: headers,
          body: body);
      final data = jsonDecode(response.body);
      if (data['message'] == "success") {
        result = "done";
        buildsByModule.setRow(data['data']);
      } else {
        result == "Something went wrong";
      }
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

  /*
-------------------------------------
        get logs
-------------------------------------
*/
  static Future<String> getLogs({required String containerName}) async {
    late String result = "none";
    final headers = {'Content-Type': 'application/json'};
    final body = jsonEncode({'container_name': containerName});
    try {
      final response = await http.post(Uri.parse("$apiHost/api/build/getlogs"),
          headers: headers, body: body);
      final data = jsonDecode(response.body);
      if (data['message'] == "success") {
        result = data['data'];
      } else {
        result == "Something went wrong";
      }
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

  /*
-------------------------------------
        test complete
-------------------------------------
*/
  static Future<String> testCompleted(
      {required List? moduleData, required List? buildData}) async {
    final List module = moduleData ?? [];
    final List build = buildData ?? [];
    late String result = "none";
    if (module == [] || build == []) {
      result = "ERROR";
    } else {
      final headers = {'Content-Type': 'application/json'};
      final body = jsonEncode({'module': module, 'build': build});
      try {
        final response = await http.post(
            Uri.parse("$apiHost/api/build/testcompleted"),
            headers: headers,
            body: body);
        final data = jsonDecode(response.body);
        if (data['message'] == "success") {
          result = data['data'];
        } else {
          result == "Something went wrong";
        }
      } catch (e) {
        result = e.toString();
      }
    }

    return result;
  }
}
