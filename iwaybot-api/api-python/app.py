from flask import Flask, jsonify, request
import psycopg2
from flask_cors import CORS
import os
import subprocess

connection = psycopg2.connect(host="localhost",database="iwaybot",user="iwaybot",password="iwaybot")


cur = connection.cursor()



app = Flask(__name__)
CORS(app)

@app.route('/api/login',methods=['POST'])
def login():
    data = request.json
    username = data['username']
    password = data['password']
    cur = connection.cursor()
    cur.execute("SELECT * FROM agents WHERE username=%s AND password=%s", (username, password))
    
    if cur.rowcount >0:
        row = cur.fetchall()
        response = {'data': row,'message': 'User logged in successfully'}
        return jsonify(response)
    else:
        response = {'message': 'Invalid username or password'}
        return jsonify(response)


@app.route('/api/module/create',methods=['POST'])
def add_module():
    module_id = request.json['module_id']
    branch = request.json['branch']
    token = request.json['token']
    odoo_version = request.json['odoo_version']
    workdir = "/home/"+os.getenv('USER')+"/workdir"
    build_count = int(0)
    username = request.json['username']
    repo_name = request.json['repo_name']
    
    cur.execute("INSERT INTO modules (module_id ,branch, token ,odoo_version, work_dir,build_count,username,repo_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",(module_id,branch,token,odoo_version,workdir,build_count,username,repo_name))
    
    connection.commit()
    if cur.rowcount > 0:
        response = {'message': 'success'}
        return jsonify(response)
        
    else:
        response = {'message': 'ERROR HAPPEND'}
        return jsonify(response), 400

@app.route('/api/module/getall',methods=['POST'])
def get_module_all():
    cur.execute("SELECT * FROM modules")
    connection.commit()
    if cur.rowcount >= 0:
        rows = cur.fetchall()
        response = {'row-count':cur.rowcount, 'data': rows,'message': 'success'}
        return jsonify(response)
        
    else:
        response = {'row-count':-1, 'data': [], 'message': 'ERROR HAPPEND'}
        return jsonify(response), 400


@app.route('/api/build/getall',methods=['POST'])
def get_build_all():
    cur.execute("SELECT * FROM builds")
    connection.commit()
    if cur.rowcount >= 0:
        rows = cur.fetchall()
        response = {'row-count':cur.rowcount, 'data': rows,'message': 'success'}
        return jsonify(response)
    else:
        response = {'row-count':-1, 'data': [], 'message': 'ERROR HAPPEND'}
        return jsonify(response), 400
    
@app.route('/api/build/getbymodule',methods=['POST'])
def get_build_by_module():
    data = request.json
    module_id = data['module_id']
    cur = connection.cursor()
    cur.execute("SELECT * FROM builds WHERE module_id=%s",(module_id,))
    if cur.rowcount >= 0:
        rows = cur.fetchall()
        response = {'row-count':cur.rowcount, 'data': rows,'message': 'success'}
        return jsonify(response)
        
    else:
        response = {'row-count':-1, 'data': [], 'message': 'ERROR HAPPEND'}
        return jsonify(response), 400

@app.route('/api/build/getlogs',methods=['POST'])
def get_logs():
    data = request.json
    container_name = data['container_name']
    command = "docker logs "+ container_name
    result = subprocess.check_output(command,shell=True,stderr=subprocess.STDOUT)
    response = {'data': result.decode("utf-8"),'message': 'success'}
    return jsonify(response)


@app.route('/api/build/testcompleted',methods=['POST'])
def get_testcompleted():
    data = request.json
    module = data['module']
    command = "curl --request POST --header 'PRIVATE-TOKEN: "+module[2]+"' 'https://gitlab.com/api/v4/projects/"+module[6]+"%2F"+module[7]+"/merge_requests' --data 'source_branch="+module[1]+"&target_branch=staging&title=adding-the-module-to-the-staging-environment'"
    result = subprocess.check_output(command,shell=True)
    response = {'data': result.decode("utf-8"),'message': 'success'}
    return jsonify(response)
if __name__ == '__main__':
    app.run(debug=True)
