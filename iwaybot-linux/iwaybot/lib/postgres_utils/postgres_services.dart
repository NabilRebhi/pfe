import 'package:iwaybot/data/builds.dart';
import 'package:iwaybot/data/builds_by_commit.dart';
import 'package:iwaybot/data/builds_by_module.dart';
import 'package:iwaybot/data/commits.dart';
import 'package:iwaybot/data/modules.dart';
import 'package:iwaybot/functions/encryption.dart';
import 'package:iwaybot/functions/get_path.dart';
import 'package:postgres/postgres.dart';
import 'dart:convert';
import 'dart:io';

class PostgresServices {
  PostgresServices();
  static ModulesData modules = ModulesData();
  static CommitsData commits = CommitsData();
  static BuildsData builds = BuildsData();
  static BuildsByModuleData buildsByModule = BuildsByModuleData();
  static BuildsByCommitData buildsByCommit = BuildsByCommitData();
/*
-------------------------------------
             save configs
-------------------------------------
*/
  static Future<String> savePostgresConfig(
      {required String host,
      required int port,
      required String username,
      required String password,
      required String databaseName}) async {
    final credentials = {
      "host": host,
      "port": port,
      "name": databaseName,
      "username": username,
      "password": password
    };
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    late String result = "none";
    var connection = PostgreSQLConnection(
      host,
      port,
      'postgres',
      username: username,
      password: password,
    );
    try {
      await connection.open();
      await connection.execute("CREATE DATABASE $databaseName");
      await connection.close();
      result = "done";
    } catch (e) {
      result = e.toString();
    }
    if (result == "done") {
      try {
        final jsonString = json.encode(credentials);
        await File('${directory.path}/credentials.json')
            .writeAsString(jsonString);
      } catch (e) {
        result = e.toString();
      }
    }
    try {
      await createAgentsTableIfNotExist();
      await createModuleTableIfNotExist();
      await createBuildsTableIfNotExist();
    } catch (e) {
      result = e.toString();
    }
    return result;
  }

/*
-------------------------------------
        check credentials
-------------------------------------
*/
  static Future<String> checkDatabaseCredentialsExistance(
      Directory directory) async {
    late String result = "none";

    GetPath.path.setPath("/home/${Platform.environment['USER']}");
    try {
      await File('${GetPath.path.getPath()}/credentials.json').readAsString();

      result = "done";
    } catch (e) {
      result = e.toString();
    }

    if (result == "done") {}

    return result;
  }

/*
-------------------------------------
        create tables
-------------------------------------
*/
  static Future<String> createAgentsTableIfNotExist() async {
    late String result = "none";

    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    try {
      await conn.open();
      await conn.query(
          "CREATE TABLE IF NOT EXISTS agents (username TEXT NOT NULL,password TEXT NOT NULL  ,role TEXT NOT NULL, PRIMARY KEY (username))");
      result = "done";
      await conn.close();
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

  static Future<String> createModuleTableIfNotExist() async {
    late String result = "none";

    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    try {
      await conn.open();
      await conn.query(
          "CREATE TABLE IF NOT EXISTS modules (module_id TEXT NOT NULL,branch TEXT NOT NULL  ,token TEXT NOT NULL,odoo_version TEXT NOT NULL,work_dir TEXT NOT NULL, build_count INTEGER NOT NULL,username TEXT NOT NULL,repo_name TEXT NOT NULL, PRIMARY KEY (module_id))");
      result = "done";
      await conn.close();
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

  static Future<String> createBuildsTableIfNotExist() async {
    late String result = "none";

    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    try {
      await conn.open();

      await conn.query(
          "CREATE TABLE IF NOT EXISTS builds (commit_id TEXT NOT NULL,module_id TEXT REFERENCES modules(module_id),status TEXT NOT NULL, build_dir TEXT NOT NULL,instance_hostname TEXT NOT NULL,instance_port TEXT NOT NULL,PRIMARY KEY (commit_id))");
      result = "done";

      await conn.close();
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

/*
-------------------------------------
           add agents
-------------------------------------
*/

  static Future<String> addAgent({
    required String username,
    required String password,
    required String role,
  }) async {
    late String result = "none";
    final String encryptedPass = Encryption.generateMd5(password);

    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    await conn.open();

    await conn.query("SELECT username FROM agents WHERE username=@id",
        substitutionValues: {"id": username}).then((value) {
      if (value.affectedRowCount != 0) {
        result = "agent exists";
      } else {
        result = "done";
      }
    });
    if (result == "done") {
      try {
        await conn.query(
          "INSERT INTO agents (username ,password, role) VALUES (@username,@password,@role)",
          substitutionValues: {
            "username": username,
            "password": encryptedPass,
            "role": role
          },
        );
        result = "done";
      } catch (e) {
        result = e.toString();
      }
    }
    await conn.close();
    return result;
  }

/*
-------------------------------------
        add modules
-------------------------------------
*/
  static Future<String> addModuleConfig(
      {required String username,
      required String repoName,
      required String branch,
      required String token,
      required String version,
      required String workDir}) async {
    late String result = "none";
    final String moduleId = "$username-$repoName";
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    await conn.open();

    await conn.query("SELECT module_id FROM modules WHERE module_id=@id",
        substitutionValues: {"id": moduleId}).then((value) {
      if (value.affectedRowCount != 0) {
        result = "module exists";
      } else {
        result = "done";
      }
    });
    if (result == "done") {
      try {
        await conn.query(
          "INSERT INTO modules (module_id ,branch, token ,odoo_version, work_dir,build_count,username,repo_name) VALUES (@module_id,@branch,@token,@odoo_version,@work_dir,@builds_count,@username,@repo_name)",
          substitutionValues: {
            "module_id": moduleId,
            "branch": branch,
            "token": token,
            "odoo_version": version,
            "work_dir": workDir,
            "builds_count": 0,
            "username": username,
            "repo_name": repoName
          },
        );
        result = "done";
      } catch (e) {
        result = e.toString();
      }
    }
    await conn.close();
    return result;
  }

/*
-------------------------------------
        get modules
-------------------------------------
*/
  static Future<String> getModules() async {
    late String result = "none";
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    await conn.open();
    try {
      await conn.query("SELECT * FROM \"modules\"").then((value) {
        if (value.affectedRowCount != 0) {
          result = "done";
          modules.setRow(value);
        } else if (value.isEmpty) {
          result = "no modules found";
        }
      });
    } catch (e) {
      result = e.toString();
    }

    await conn.close();

    return result;
  }

/*
-------------------------------------
        get Commits
-------------------------------------
*/

  static Future<String> getCommits({required String moduleId}) async {
    late String result = "none";
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    await conn.open();
    try {
      await conn.query(
          "SELECT commit_id FROM \"builds\" WHERE module_id=@module_id",
          substitutionValues: {"module_id": moduleId}).then((value) {
        if (value.affectedRowCount != 0) {
          result = "done";
          commits.setRow(value);
        } else if (value.isEmpty) {
          result = "no commits found";
          commits.setRow([]);
        }
      });
    } catch (e) {
      result = e.toString();
    }

    await conn.close();

    return result;
  }

/*
-------------------------------------
        get Builds
-------------------------------------
*/

  static Future<String> getBuilds() async {
    late String result = "none";
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    await conn.open();
    try {
      await conn.query("SELECT * FROM \"builds\" ").then((value) {
        if (value.affectedRowCount != 0) {
          result = "done";
          builds.setRow(value);
        } else if (value.isEmpty) {
          result = "no builds found";
        }
      });
    } catch (e) {
      result = e.toString();
    }

    await conn.close();

    return result;
  }
  /*
-------------------------------------
        get Builds by module
-------------------------------------
*/

  static Future<String> getBuildsByModule({required String moduleId}) async {
    late String result = "none";
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    await conn.open();
    try {
      await conn.query("SELECT * FROM \"builds\" WHERE module_id=@module_id",
          substitutionValues: {"module_id": moduleId}).then((value) {
        if (value.affectedRowCount != 0) {
          result = "done";
          buildsByModule.setRow(value);
        } else if (value.isEmpty) {
          result = "no builds found";
        }
      });
    } catch (e) {
      result = e.toString();
    }

    await conn.close();

    return result;
  }
  /*
-------------------------------------
        add Builds
-------------------------------------
*/

  static Future<String> addBuilds(
      {required String commitId,
      required String moduleId,
      required String status,
      required String buildDir,
      required String instanceHostname,
      required String instancePort}) async {
    late String result = "none";
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    await conn.open();
    try {
      await conn.query(
          "INSERT INTO \"builds\" (commit_id ,module_id ,status, build_dir ,instance_hostname, instance_port) VALUES (@commit_id ,@module_id , @status, @build_dir ,@instance_hostname, @instance_port)",
          substitutionValues: {
            "commit_id": commitId,
            "module_id": moduleId,
            "status": status,
            "build_dir": buildDir,
            "instance_hostname": instanceHostname,
            "instance_port": instancePort
          });
      await conn.query(
          "UPDATE public.modules SET build_count = build_count + 1 WHERE module_id = @module_id",
          substitutionValues: {"module_id": moduleId});
    } catch (e) {
      result = e.toString();
    }

    await conn.close();

    return result;
  }

  /*
-------------------------------------
        update build
-------------------------------------
*/

  static Future<String> updateBuilds({required String commitId}) async {
    late String result = "none";
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    await conn.open();
    try {
      await conn.query(
          "UPDATE public.builds SET status='done' WHERE commit_id = @commit_id",
          substitutionValues: {"commit_id": commitId});
    } catch (e) {
      result = e.toString();
    }

    await conn.close();

    return result;
  }
  /*
-------------------------------------
        get Builds by commit
-------------------------------------
*/

  static Future<String> getBuildsByCommit({required String commitId}) async {
    late String result = "none";
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final jsonString =
        await File('${directory.path}/credentials.json').readAsString();
    final credentials = json.decode(jsonString);
    var conn = PostgreSQLConnection(
        credentials['host'], credentials['port'], credentials['name'],
        username: credentials['username'], password: credentials['password']);
    await conn.open();
    try {
      await conn.query("SELECT * FROM \"builds\" WHERE commit_id=@commit_id",
          substitutionValues: {"commit_id": commitId}).then((value) {
        if (value.affectedRowCount != 0) {
          result = "done";
          buildsByCommit.setRow(value);
        } else if (value.isEmpty) {
          result = "no builds found";
        }
      });
    } catch (e) {
      result = e.toString();
    }

    await conn.close();

    return result;
  }
}
