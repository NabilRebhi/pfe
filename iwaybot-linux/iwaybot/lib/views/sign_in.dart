// ignore_for_file: use_build_context_synchronously

import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:iwaybot/functions/admin_credentials.dart';
import 'package:iwaybot/views/dashboard.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final TextEditingController _adminUsername = TextEditingController();
  final TextEditingController _adminPassword = TextEditingController();
  late bool isHidden = true;
  late bool check;
  late String result = "none";
  late String user = "none";
  late String pass = "none";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 30,
          ),
          const Text(
            "I-WAY BOT",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
          ),
          const SizedBox(
            height: 30,
          ),
          Center(
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.4,
              width: MediaQuery.of(context).size.width * 0.8,
              child: DecoratedBox(
                decoration: const BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                ),
                child: SingleChildScrollView(
                  child: Form(
                    key: _form,
                    child: Center(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 30,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              const Text(
                                "Admin username :                  ",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              ConstrainedBox(
                                constraints: const BoxConstraints(
                                  maxWidth: 300,
                                ),
                                child: TextFormField(
                                  controller: _adminUsername,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    prefixIcon: const Icon(Icons.person),
                                    filled: true,
                                    hintStyle:
                                        TextStyle(color: Colors.grey[800]),
                                    fillColor: Colors.white70,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              const Text(
                                "Admin password :                  ",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              ConstrainedBox(
                                constraints: const BoxConstraints(
                                  maxWidth: 300,
                                ),
                                child: TextFormField(
                                  controller: _adminPassword,
                                  obscureText: isHidden,
                                  validator: (validator) {
                                    if (validator!.isEmpty) {
                                      return 'Empty !';
                                    } else if (validator.length < 8) {
                                      return 'Password length should be 8 or more!';
                                    }

                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    prefixIcon: const Icon(Icons.password),
                                    suffixIcon: isHidden
                                        ? IconButton(
                                            onPressed: () {
                                              setState(() {
                                                isHidden = !isHidden;
                                              });
                                            },
                                            icon: const Icon(
                                                Icons.visibility_off))
                                        : IconButton(
                                            onPressed: () {
                                              setState(() {
                                                isHidden = !isHidden;
                                              });
                                            },
                                            icon: const Icon(Icons.visibility)),
                                    filled: true,
                                    hintStyle:
                                        TextStyle(color: Colors.grey[800]),
                                    fillColor: Colors.white70,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              DecoratedBox(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.white,
                                ),
                                child: TextButton(
                                  onPressed: () async {
                                    setState(() {
                                      check = _form.currentState!.validate();
                                    });
                                    if (check == true) {
                                      //register function
                                      await AdminCredentials
                                              .getAdminCredentials(
                                                  username: _adminUsername.text,
                                                  password: _adminPassword.text)
                                          .then((value) {
                                        setState(() {
                                          result = value;
                                        });
                                      });
                                      if (result == "done") {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const Dashboard()),
                                        );
                                      } else {
                                        return alert(
                                          context,
                                          title: const Text(
                                              "Something went wrong!"),
                                          content: Text(result),
                                          textOK: const Text("GOT IT"),
                                        );
                                      }
                                    }
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 30,
                                      vertical: 10,
                                    ),
                                    child: Text(
                                      "Submit",
                                      style: TextStyle(color: Colors.blue),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
