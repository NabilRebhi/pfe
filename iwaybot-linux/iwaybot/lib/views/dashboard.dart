// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'dart:convert';

import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:iwaybot/functions/build_module.dart';
import 'package:iwaybot/functions/containes_logs.dart';
import 'package:iwaybot/functions/testin_completed.dart';
import 'package:iwaybot/postgres_utils/postgres_services.dart';
import 'package:iwaybot/views/settings.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  late List<Widget> list = [];
  late int usedPort = 2000;
  late Timer _refreshTimer;
  late List<Map<String, dynamic>> data = [
    {"modules number": 0},
    {"commits number": 0}
  ];

  late String getBuildsByModuleResult = "none";

  late String getBuildsResult = "none";
  late String getCommitsResult = "none";

  Color buildColor(String value) {
    if (value == "done") {
      return Colors.green;
    } else if (value == "error") {
      return Colors.red;
    } else {
      return Colors.black;
    }
  }

  Future<void> _refresh() async {
    late int usedPort1 = usedPort;
    late String getModulesResult = "none";
    await PostgresServices.getModules().then((value) {
      getModulesResult = value;
    });
    if (getModulesResult == "done") {
      setState(() {
        data[0]['modules number'] = PostgresServices.modules.getRow()!.length;
      });

      await PostgresServices.getBuilds()
          .then((value) => {getBuildsResult = value});

      for (int moduleIndex = 0;
          moduleIndex < data[0]['modules number'];
          moduleIndex++) {
        await PostgresServices.getBuildsByModule(
                moduleId: PostgresServices.modules
                    .getRow()!
                    .elementAt(moduleIndex)
                    .elementAt(0))
            .then((value) {
          getBuildsByModuleResult = value;
        });
        if (getBuildsByModuleResult == "done") {
          /*
            ----------------------------------------------------------------------------
                   check for new commits and if there is one, will initiate a build
            ----------------------------------------------------------------------------
            */
          final response = await http.get(
              Uri.parse(
                  'https://gitlab.com/api/v4/projects/${PostgresServices.modules.getRow()!.elementAt(moduleIndex).elementAt(6)}%2F${PostgresServices.modules.getRow()!.elementAt(moduleIndex).elementAt(7)}/repository/commits?ref_name=${PostgresServices.modules.getRow()!.elementAt(moduleIndex).elementAt(1)}&per_page=1'),
              headers: {
                'PRIVATE-TOKEN':
                    '${PostgresServices.modules.getRow()!.elementAt(moduleIndex).elementAt(2)}'
              });
          final List<dynamic> commits = jsonDecode(response.body);
          if (commits.isNotEmpty) {
            late String result = "none";
            late String commitsres = "none";
            await PostgresServices.getCommits(
                    moduleId: PostgresServices.modules
                        .getRow()!
                        .elementAt(moduleIndex)
                        .elementAt(0))
                .then((value) {
              commitsres = value;
            });
            if (commitsres == "done") {
              data[1]['commits number'] =
                  PostgresServices.commits.getRow()!.length;

              for (int commitNumber = 0;
                  commitNumber < data[1]['commits number'];
                  commitNumber++) {
                if (PostgresServices.commits
                    .getRow()!
                    .elementAt(commitNumber)
                    .contains(commits.first['short_id'])) {
                  result = "found";
                }
              }
            }
            if (result != "found") {
              /*
                  --------------------------------------------------------------------------
                          run the build and wait till it end to store the result in the DB
                  --------------------------------------------------------------------------
                  */
              await PostgresServices.addBuilds(
                  commitId: commits.first['short_id'],
                  moduleId: PostgresServices.modules
                      .getRow()!
                      .elementAt(moduleIndex)
                      .elementAt(0),
                  status: "testing",
                  buildDir: (PostgresServices.modules
                          .getRow()!
                          .elementAt(moduleIndex)
                          .elementAt(4) +
                      "/${commits.first['short_id']}"),
                  instanceHostname: "192.168.8.122",
                  instancePort: usedPort1.toString());
              /*
              ----------------------------------
                        run the build
              ----------------------------------
              */
              usedPort1 = usedPort1 + 1;
              usedPort = usedPort + 1;
              await PostgresServices.getBuildsByCommit(
                  commitId: commits.first['short_id']);
              await Build.prepareEnv(
                  moduleData:
                      PostgresServices.modules.getRow()!.elementAt(moduleIndex),
                  buildData:
                      PostgresServices.buildsByCommit.getRow()!.elementAt(0));
              await Future.delayed(const Duration(minutes: 1));
              await PostgresServices.updateBuilds(
                  commitId: commits.first['short_id']);
            }
          }
        } else if (getBuildsByModuleResult == "no builds found") {
          /*
            ----------------------------------------------------------------------------
                   check for new commits and if there is one, will initiate a build
            ----------------------------------------------------------------------------
            */
          final response = await http.get(
              Uri.parse(
                  'https://gitlab.com/api/v4/projects/${PostgresServices.modules.getRow()!.elementAt(moduleIndex).elementAt(6)}%2F${PostgresServices.modules.getRow()!.elementAt(moduleIndex).elementAt(7)}/repository/commits?ref_name=${PostgresServices.modules.getRow()!.elementAt(moduleIndex).elementAt(1)}&per_page=1'),
              headers: {
                'PRIVATE-TOKEN':
                    '${PostgresServices.modules.getRow()!.elementAt(moduleIndex).elementAt(2)}'
              });
          final List<dynamic> commits = jsonDecode(response.body);
          if (commits.isNotEmpty) {
            await PostgresServices.addBuilds(
                commitId: commits.first['short_id'],
                moduleId: PostgresServices.modules
                    .getRow()!
                    .elementAt(moduleIndex)
                    .elementAt(0),
                status: "testing",
                buildDir: (PostgresServices.modules
                        .getRow()!
                        .elementAt(moduleIndex)
                        .elementAt(4) +
                    "/${commits.first['short_id']}"),
                instanceHostname: "192.168.8.122",
                instancePort: usedPort1.toString());
            /*
              ----------------------------------
                        run the build
              ----------------------------------
              */
            usedPort1 = usedPort1 + 1;
            usedPort = usedPort + 1;
            await PostgresServices.getBuildsByCommit(
                commitId: commits.first['short_id']);
            await Build.prepareEnv(
                moduleData:
                    PostgresServices.modules.getRow()!.elementAt(moduleIndex),
                buildData:
                    PostgresServices.buildsByCommit.getRow()!.elementAt(0));
            await Future.delayed(const Duration(minutes: 1));
            await PostgresServices.updateBuilds(
                commitId: commits.first['short_id']);
          }
        }
      }

      buildWidgetsList(context, data[0]['modules number']);
    }
  }

  Widget row1(context, String value, String host, String port) {
    late Widget r = const SizedBox();
    final Uri url = Uri.parse("http://$host:$port");
    if (value == "testing") {
      r = const Text("creating instance...");
    } else if (value == "done") {
      r = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("Visit:  "),
          TextButton(
              onPressed: () async {
                if (await canLaunchUrl(url)) {
                  await launchUrl(url);
                } else {
                  return alert(
                    context,
                    title: const Text("Something went wrong!"),
                    content: const Text("Could not launch"),
                    textOK: const Text("GOT IT"),
                  );
                }
              },
              child: const Text("->HERE<-"))
        ],
      );
    } else {
      r = const Text("ERROR!!");
    }
    return r;
  }

  Widget row2(context, String value, String containerName) {
    late Widget r = const SizedBox();
    if (value == "done") {
      r = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("Logs:  "),
          TextButton(
              onPressed: () async {
                await OdooLogs.getLogs(containerName: containerName)
                    .then((value) {
                  return alert(
                    context,
                    title: Text("$containerName Logs"),
                    content: SingleChildScrollView(child: Text(value)),
                    textOK: const Text("GOT IT"),
                  );
                });
              },
              child: const Text("->HERE<-"))
        ],
      );
    }
    return r;
  }

  Widget row3(context, String value, List? module, List? build) {
    late Widget r = const SizedBox();
    if (value == "done") {
      r = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("Success?: "),
          TextButton(
              onPressed: () async {
                //function that triggers the next pipeline in Jenkins
                await Test.completed(moduleData: module);
              },
              child: const Text("->Yes<-"))
        ],
      );
    }
    return r;
  }

  void buildWidgetsList(context, int data) async {
    late List<Widget> l = [];
    if (getBuildsResult == "done") {
      for (int i = 0; i < data; i++) {
        await PostgresServices.getBuildsByModule(
            moduleId:
                PostgresServices.modules.getRow()!.elementAt(i).elementAt(0));

        l.add(SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.25,
              width: MediaQuery.of(context).size.width * 0.25,
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.008,
                  MediaQuery.of(context).size.height * 0.008,
                  MediaQuery.of(context).size.width * 0.008,
                  MediaQuery.of(context).size.height * 0.008,
                ),
                child: DecoratedBox(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("Module Name:"),
                      Text(
                          "${PostgresServices.modules.getRow()!.elementAt(i).elementAt(7)}"),
                      const Text("Branch:"),
                      Text(
                          "${PostgresServices.modules.getRow()!.elementAt(i).elementAt(1)}"),
                    ],
                  ),
                ),
              ),
            ),
            for (int j = 0;
                j <
                    PostgresServices.modules
                        .getRow()!
                        .elementAt(i)
                        .elementAt(5);
                j++)
              (SizedBox(
                height: MediaQuery.of(context).size.height * 0.25,
                width: MediaQuery.of(context).size.width * 0.25,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.008,
                    MediaQuery.of(context).size.height * 0.008,
                    MediaQuery.of(context).size.width * 0.008,
                    MediaQuery.of(context).size.height * 0.008,
                  ),
                  child: DecoratedBox(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Commit ID: ${PostgresServices.buildsByModule.getRow()!.elementAt(j).elementAt(0)}",
                          style: TextStyle(
                              color: buildColor(PostgresServices.buildsByModule
                                  .getRow()!
                                  .elementAt(j)
                                  .elementAt(2))),
                        ),
                        row1(
                            context,
                            PostgresServices.buildsByModule
                                .getRow()!
                                .elementAt(j)
                                .elementAt(2),
                            PostgresServices.buildsByModule
                                .getRow()!
                                .elementAt(j)
                                .elementAt(4),
                            PostgresServices.buildsByModule
                                .getRow()!
                                .elementAt(j)
                                .elementAt(5)),
                        row2(
                            context,
                            PostgresServices.buildsByModule
                                .getRow()!
                                .elementAt(j)
                                .elementAt(2),
                            "${PostgresServices.buildsByModule.getRow()!.elementAt(j).elementAt(0)}_odoo"),
                        row3(
                          context,
                          PostgresServices.buildsByModule
                              .getRow()!
                              .elementAt(j)
                              .elementAt(2),
                          PostgresServices.modules.getRow()!.elementAt(i),
                          PostgresServices.builds.getRow()!.elementAt(j),
                        )
                      ],
                    ),
                  ),
                ),
              ))
          ]),
        ));
      }
    }
    setState(() {
      list = l;
    });
  }

  @override
  void initState() {
    super.initState();
    _refreshTimer = Timer.periodic(const Duration(seconds: 10), (timer) {
      _refresh();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _refreshTimer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Dashboard"),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Settings()),
                );
              },
              icon: const Icon(Icons.settings)),
        ],
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: const Color.fromARGB(103, 0, 0, 0),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: list,
          ),
        ),
      ),
    );
  }
}
