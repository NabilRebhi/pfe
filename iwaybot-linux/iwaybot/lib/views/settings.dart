// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'dart:io';

import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:iwaybot/components/modules_widgets.dart';
import 'package:iwaybot/functions/get_path.dart';
import 'package:iwaybot/postgres_utils/postgres_services.dart';
import 'package:iwaybot/views/dashboard.dart';

enum Role { developer, tester }

class Settings extends StatefulWidget {
  const Settings({super.key});

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final TextEditingController _adminOldPass = TextEditingController();
  final TextEditingController _adminNewPassword = TextEditingController();
  final TextEditingController _agentUsername = TextEditingController();
  final TextEditingController _agentPassword = TextEditingController();
  final TextEditingController _adminNewPasswordRepeated =
      TextEditingController();
  final TextEditingController _repoUsername = TextEditingController();
  final TextEditingController _repoName = TextEditingController();
  final TextEditingController _token = TextEditingController();
  final TextEditingController _branch = TextEditingController();
  final TextEditingController _version = TextEditingController();
  final GlobalKey<FormState> _formBuild = GlobalKey<FormState>();
  final GlobalKey<FormState> _formAdmin = GlobalKey<FormState>();
  final GlobalKey<FormState> _formAgents = GlobalKey<FormState>();
  late bool check;
  late bool isHidden = true;
  late bool isHidden1 = true;
  late bool isHidden2 = true;
  late bool isHidden3 = true;
  late String id = "build";
  late String result = "none";
  late String workDir = GetPath.wDir.getPath() ?? '';

  late Role _selectedRole = Role.developer;
  late String _selectedRoleText = "developer";

  late Timer _refreshTimer;
  late String getTerminalsResult = "none";
  late List<TableRow> rows = [];
  late int length = 0;

  Future<void> _refresh() async {
    await PostgresServices.getModules().then((value) {
      setState(() {
        getTerminalsResult = value;
      });
    });
    if (getTerminalsResult == "done") {
      setState(() {
        length = PostgresServices.modules.getRow()!.length;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    // this to refresh the list every 5 seconds
    _refreshTimer = Timer.periodic(const Duration(seconds: 5), (timer) {
      _refresh();
    });
  }

  @override
  void dispose() {
    // Cancel the timer when the widget is disposed
    _refreshTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Settings"),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Dashboard()),
                );
              },
              icon: const Icon(Icons.dashboard)),
        ],
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: const Color.fromARGB(103, 0, 0, 0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            //left side
            SizedBox(
              height: MediaQuery.of(context).size.height * 1,
              width: MediaQuery.of(context).size.width * 0.3,
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.008,
                  MediaQuery.of(context).size.height * 0.008,
                  MediaQuery.of(context).size.width * 0.008,
                  MediaQuery.of(context).size.height * 0.008,
                ),
                child: DecoratedBox(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.05,
                      ),
                      DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.black,
                        ),
                        child: TextButton(
                          onPressed: () {
                            setState(() {
                              id = "modules";
                            });
                          },
                          child: SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                            width: MediaQuery.of(context).size.width * 0.25,
                            child: const Center(
                              child: Text(
                                "Modules",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.05,
                      ),
                      DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.black,
                        ),
                        child: TextButton(
                          onPressed: () {
                            setState(() {
                              id = "build";
                            });
                          },
                          child: SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                            width: MediaQuery.of(context).size.width * 0.25,
                            child: const Center(
                              child: Text(
                                "Build settings",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.05,
                      ),
                      DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.black,
                        ),
                        child: TextButton(
                          onPressed: () {
                            setState(() {
                              id = "admin";
                            });
                          },
                          child: SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                            width: MediaQuery.of(context).size.width * 0.25,
                            child: const Center(
                              child: Text(
                                "Administrator",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.05,
                      ),
                      DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.black,
                        ),
                        child: TextButton(
                          onPressed: () {
                            setState(() {
                              id = "agents";
                            });
                          },
                          child: SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                            width: MediaQuery.of(context).size.width * 0.25,
                            child: const Center(
                              child: Text(
                                "agents",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            //right side
            if (id == "build")
              (SizedBox(
                height: MediaQuery.of(context).size.height * 1,
                width: MediaQuery.of(context).size.width * 0.7,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(
                    0,
                    MediaQuery.of(context).size.height * 0.008,
                    MediaQuery.of(context).size.width * 0.008,
                    MediaQuery.of(context).size.height * 0.008,
                  ),
                  child: DecoratedBox(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: SingleChildScrollView(
                      child: Form(
                        key: _formBuild,
                        child: Column(
                          children: [
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "GitLab username:                                                 ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width * 0.3,
                                  ),
                                  child: TextFormField(
                                    controller: _repoUsername,
                                    validator: (validator) {
                                      if (validator!.isEmpty) {
                                        return 'Empty !';
                                      }

                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      hintText: "Username",
                                      filled: true,
                                      hintStyle:
                                          TextStyle(color: Colors.grey[800]),
                                      fillColor: Colors.white70,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "GitLab repository name:                                     ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width * 0.3,
                                  ),
                                  child: TextFormField(
                                    controller: _repoName,
                                    validator: (validator) {
                                      if (validator!.isEmpty) {
                                        return 'Empty !';
                                      }

                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      hintText: "Repository Name",
                                      filled: true,
                                      hintStyle:
                                          TextStyle(color: Colors.grey[800]),
                                      fillColor: Colors.white70,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "Branch:                                                                      ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width * 0.3,
                                  ),
                                  child: TextFormField(
                                    controller: _branch,
                                    validator: (validator) {
                                      if (validator!.isEmpty) {
                                        return 'Empty !';
                                      }

                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      hintText: "Exemple: module_testing",
                                      filled: true,
                                      hintStyle:
                                          TextStyle(color: Colors.grey[800]),
                                      fillColor: Colors.white70,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "GitLab Token:                                                          ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width * 0.3,
                                  ),
                                  child: TextFormField(
                                    controller: _token,
                                    validator: (validator) {
                                      if (validator!.isEmpty) {
                                        return 'Empty !';
                                      }

                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      filled: true,
                                      hintText: "Token",
                                      hintStyle:
                                          TextStyle(color: Colors.grey[800]),
                                      fillColor: Colors.white70,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "Odoo version:                                                           ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width * 0.3,
                                  ),
                                  child: TextFormField(
                                    controller: _version,
                                    validator: (validator) {
                                      if (validator!.isEmpty) {
                                        return 'Empty !';
                                      }

                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      hintText: "Exemple: 13",
                                      filled: true,
                                      hintStyle:
                                          TextStyle(color: Colors.grey[800]),
                                      fillColor: Colors.white70,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            DecoratedBox(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.green,
                              ),
                              child: TextButton(
                                onPressed: () async {
                                  setState(() {
                                    check = _formBuild.currentState!.validate();
                                  });
                                  if (check == true) {
                                    await PostgresServices.addModuleConfig(
                                            username: _repoUsername.text,
                                            repoName: _repoName.text,
                                            branch: _branch.text,
                                            token: _token.text,
                                            version: _version.text,
                                            workDir:
                                                "/home/${Platform.environment['USER']}/workdir")
                                        .then((value) {
                                      setState(() {
                                        result = value;
                                      });
                                    });
                                    if (result == "done") {
                                      return alert(
                                        context,
                                        title: const Text(
                                            "Module saved successfully!"),
                                        content: Text(result),
                                        textOK: const Text("GOT IT"),
                                      );
                                    } else {
                                      return alert(
                                        context,
                                        title: const Text(
                                            "Something went wrong !!"),
                                        content: Text(result),
                                        textOK: const Text("GOT IT"),
                                      );
                                    }
                                  }
                                },
                                child: SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.05,
                                  width:
                                      MediaQuery.of(context).size.width * 0.25,
                                  child: const Center(
                                    child: Text(
                                      "Save Settings",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              )),
            if (id == "admin")
              (SizedBox(
                height: MediaQuery.of(context).size.height * 1,
                width: MediaQuery.of(context).size.width * 0.7,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(
                    0,
                    MediaQuery.of(context).size.height * 0.008,
                    MediaQuery.of(context).size.width * 0.008,
                    MediaQuery.of(context).size.height * 0.008,
                  ),
                  child: DecoratedBox(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: SingleChildScrollView(
                      child: Form(
                        key: _formAdmin,
                        child: Column(
                          children: [
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "Old password:                                  ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width * 0.3,
                                  ),
                                  child: TextFormField(
                                    controller: _adminOldPass,
                                    validator: (validator) {
                                      if (validator!.isEmpty) {
                                        return 'Empty !';
                                      }

                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      prefixIcon: const Icon(Icons.password),
                                      suffixIcon: isHidden
                                          ? IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  isHidden = !isHidden;
                                                });
                                              },
                                              icon: const Icon(
                                                  Icons.visibility_off))
                                          : IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  isHidden = !isHidden;
                                                });
                                              },
                                              icon:
                                                  const Icon(Icons.visibility)),
                                      filled: true,
                                      hintStyle:
                                          TextStyle(color: Colors.grey[800]),
                                      fillColor: Colors.white70,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "New password:                                  ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width * 0.3,
                                  ),
                                  child: TextFormField(
                                    controller: _adminNewPassword,
                                    validator: (validator) {
                                      if (validator!.isEmpty) {
                                        return 'Empty !';
                                      } else if (validator.length < 8) {
                                        return 'Password length should be 8 or more!';
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      prefixIcon: const Icon(Icons.password),
                                      suffixIcon: isHidden1
                                          ? IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  isHidden1 = !isHidden1;
                                                });
                                              },
                                              icon: const Icon(
                                                  Icons.visibility_off))
                                          : IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  isHidden1 = !isHidden1;
                                                });
                                              },
                                              icon:
                                                  const Icon(Icons.visibility)),
                                      filled: true,
                                      hintStyle:
                                          TextStyle(color: Colors.grey[800]),
                                      fillColor: Colors.white70,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "Repeat new password:                    ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width * 0.3,
                                  ),
                                  child: TextFormField(
                                    controller: _adminNewPasswordRepeated,
                                    validator: (validator) {
                                      if (validator!.isEmpty) {
                                        return 'Empty !';
                                      }

                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      prefixIcon: const Icon(Icons.password),
                                      suffixIcon: isHidden2
                                          ? IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  isHidden2 = !isHidden2;
                                                });
                                              },
                                              icon: const Icon(
                                                  Icons.visibility_off))
                                          : IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  isHidden2 = !isHidden2;
                                                });
                                              },
                                              icon:
                                                  const Icon(Icons.visibility)),
                                      filled: true,
                                      hintStyle:
                                          TextStyle(color: Colors.grey[800]),
                                      fillColor: Colors.white70,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            DecoratedBox(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.blue,
                              ),
                              child: TextButton(
                                onPressed: () {},
                                child: SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.05,
                                  width:
                                      MediaQuery.of(context).size.width * 0.25,
                                  child: const Center(
                                    child: Text(
                                      "Save credentials",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              )),
            if (id == "agents")
              (SizedBox(
                height: MediaQuery.of(context).size.height * 1,
                width: MediaQuery.of(context).size.width * 0.7,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(
                    0,
                    MediaQuery.of(context).size.height * 0.008,
                    MediaQuery.of(context).size.width * 0.008,
                    MediaQuery.of(context).size.height * 0.008,
                  ),
                  child: DecoratedBox(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Form(
                      key: _formAgents,
                      child: Column(
                        children: [
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              const Text(
                                "username:                                      ",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                  maxWidth:
                                      MediaQuery.of(context).size.width * 0.3,
                                ),
                                child: TextFormField(
                                  controller: _agentUsername,
                                  validator: (validator) {
                                    if (validator!.isEmpty) {
                                      return 'Empty !';
                                    }

                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    filled: true,
                                    hintStyle:
                                        TextStyle(color: Colors.grey[800]),
                                    fillColor: Colors.white70,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              const Text(
                                "Password:                                      ",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                  maxWidth:
                                      MediaQuery.of(context).size.width * 0.3,
                                ),
                                child: TextFormField(
                                  controller: _agentPassword,
                                  obscureText: isHidden,
                                  validator: (validator) {
                                    if (validator!.isEmpty) {
                                      return 'Empty !';
                                    } else if (validator.length < 8) {
                                      return 'Password length should be 8 or more!';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    filled: true,
                                    prefixIcon: const Icon(Icons.password),
                                    suffixIcon: isHidden3
                                        ? IconButton(
                                            onPressed: () {
                                              setState(() {
                                                isHidden3 = !isHidden3;
                                              });
                                            },
                                            icon: const Icon(
                                                Icons.visibility_off))
                                        : IconButton(
                                            onPressed: () {
                                              setState(() {
                                                isHidden3 = !isHidden3;
                                              });
                                            },
                                            icon: const Icon(Icons.visibility)),
                                    hintStyle:
                                        TextStyle(color: Colors.grey[800]),
                                    fillColor: Colors.white70,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                  maxWidth:
                                      MediaQuery.of(context).size.width * 0.3,
                                ),
                                child: RadioListTile(
                                    title: const Text("Developer"),
                                    value: Role.developer,
                                    groupValue: _selectedRole,
                                    onChanged: (value) {
                                      setState(() {
                                        _selectedRole = value!;
                                      });
                                    }),
                              ),
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                  maxWidth:
                                      MediaQuery.of(context).size.width * 0.3,
                                ),
                                child: RadioListTile(
                                    title: const Text("Tester"),
                                    value: Role.tester,
                                    groupValue: _selectedRole,
                                    onChanged: (value) {
                                      setState(() {
                                        _selectedRole = value!;
                                      });
                                    }),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                          ),
                          DecoratedBox(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.blue,
                            ),
                            child: TextButton(
                              onPressed: () async {
                                setState(() {
                                  check = _formAgents.currentState!.validate();
                                });
                                if (check == true) {
                                  if (_selectedRole == Role.developer) {
                                    setState(() {
                                      _selectedRoleText = "developer";
                                    });
                                  } else {
                                    setState(() {
                                      _selectedRoleText = "tester";
                                    });
                                  }
                                  await PostgresServices.addAgent(
                                          username: _agentUsername.text,
                                          password: _agentPassword.text,
                                          role: _selectedRoleText)
                                      .then((value) {
                                    setState(() {
                                      result = value;
                                    });
                                  });
                                  if (result == "done") {
                                    return alert(
                                      context,
                                      title: const Text(
                                          "Agent Added successfully!"),
                                      content: Text(result),
                                      textOK: const Text("GOT IT"),
                                    );
                                  } else {
                                    return alert(
                                      context,
                                      title: const Text("EROOR!"),
                                      content: Text(result),
                                      textOK: const Text("GOT IT"),
                                    );
                                  }
                                }
                              },
                              child: SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.05,
                                width: MediaQuery.of(context).size.width * 0.25,
                                child: const Center(
                                  child: Text(
                                    "Save credentials",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )),
            if (id == "modules")
              (SizedBox(
                height: MediaQuery.of(context).size.height * 1,
                width: MediaQuery.of(context).size.width * 0.7,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(
                    0,
                    MediaQuery.of(context).size.height * 0.008,
                    MediaQuery.of(context).size.width * 0.008,
                    MediaQuery.of(context).size.height * 0.008,
                  ),
                  child: const DecoratedBox(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Modules(),
                  ),
                ),
              )),
          ],
        ),
      ),
    );
  }
}
