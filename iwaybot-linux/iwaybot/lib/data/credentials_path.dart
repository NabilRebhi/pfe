class PathData {
  PathData();

  String? _path;

  String? getPath() {
    return _path;
  }

  setPath(String? path) {
    _path = path;
  }
}
