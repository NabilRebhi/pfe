class BuildsByCommitData {
  BuildsByCommitData();

  List? _rowForBuilds;

  List? getRow() {
    return _rowForBuilds;
  }

  setRow(List? rowForBuilds) {
    _rowForBuilds = rowForBuilds;
  }
}
