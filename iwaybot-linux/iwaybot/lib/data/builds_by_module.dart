class BuildsByModuleData {
  BuildsByModuleData();

  List? _rowForBuilds;

  List? getRow() {
    return _rowForBuilds;
  }

  setRow(List? rowForBuilds) {
    _rowForBuilds = rowForBuilds;
  }
}
