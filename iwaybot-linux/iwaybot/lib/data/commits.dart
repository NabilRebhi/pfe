class CommitsData {
  CommitsData();

  List? _rowForCommits;

  List? getRow() {
    return _rowForCommits;
  }

  setRow(List? rowForCommits) {
    _rowForCommits = rowForCommits;
  }
}
