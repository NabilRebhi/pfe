import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';

class Alerts {
  Alerts();

  static credentialsDirectoryEroor(context) {
    return alert(
      context,
      title: const Text("EROOR !"),
      content: const Text(
          "You need to Choose where to store your credentials files"),
      textOK: const Text("GOT IT"),
    );
  }

  static workingDirectoryEroor(context) {
    return alert(
      context,
      title: const Text("ERROR !"),
      content: const Text("You need to Choose The working directory"),
      textOK: const Text("GOT IT"),
    );
  }
}
