import 'dart:async';
import 'dart:io';

class Build {
  Build();

  static Future<String> prepareEnv(
      {required List? moduleData, required List? buildData}) async {
    final List module = moduleData ?? [];
    final List build = buildData ?? [];
    late String result = "none";

    if (module.isNotEmpty && build.isNotEmpty) {
      final String repoUrl =
          "git@gitlab.com:${module.elementAt(6)}/${module.elementAt(7)}.git";
      /*---------------------------------------
              create commit directory
      ----------------------------------------*/

      final Directory buildDir =
          Directory('${module.elementAt(4)}/${build.elementAt(0)}');
      buildDir.createSync();

      /*---------------------------------------
              create addons  directory
      ----------------------------------------*/

      final Directory addonsDir = Directory('${buildDir.path}/addons');
      addonsDir.createSync();

      /*---------------------------------------
                  clone module repo
      ----------------------------------------*/

      ProcessResult processResult = await Process.run('git',
          ['clone', '--branch', module.elementAt(1), repoUrl, addonsDir.path]);
      if (processResult.exitCode != 0) {
        return "failed to clone repository";
      }

      /*---------------------------------------
              generate odoo config file
      ----------------------------------------*/
      /*final Directory configDir = Directory('${buildDir.path}/config');
      configDir.createSync();
      final File configFile = File('${configDir.path}/odoo.conf');
      const String configContents = '''
[options]
addons_path = /mnt/extra-addons
admin_passwd = admin
auto_reload = True
data_dir = /var/lib/odoo
db_host = db
db_port = 5432
db_user = odoo
db_password = odoo
db_name = odoo
''';

      configFile.writeAsStringSync(configContents);*/
      final Directory configDir = Directory('${buildDir.path}/config');
      configDir.createSync();

      ProcessResult instanceResult0 = await Process.run(
          'mv', ['${addonsDir.path}/odooconf/odoo.conf', '${configDir.path}/odoo.conf']);
      if (instanceResult0.exitCode != 0) {
        return instanceResult0.stderr;
      }
      ProcessResult instanceResult1 = await Process.run('mv',
          ['${addonsDir.path}/Dockerfile', '${buildDir.path}/Dockerfile']);
      if (instanceResult1.exitCode != 0) {
        return instanceResult1.stderr;
      }

      /*---------------------------------------
                generate compose file
      ----------------------------------------*/
      final File composeFile = File('${buildDir.path}/docker-compose.yaml');
      final String contents = '''
version: '3'
services:
  web:
    build: .
    container_name: ${build.elementAt(0)}_odoo
    depends_on:
      - db
    ports:
      - "${build.elementAt(5)}:8069"
    volumes:
      - odoo-web-data:/var/lib/odoo
      - ./config:/etc/odoo
      - ./addons:/mnt/extra-addons
    environment:
      - Host=db
      - USER=odoo
      - PASSWORD=odoo
    #command: ["--init=odoo", "--without-demo=1"]
  db:
    image: postgres
    container_name: ${build.elementAt(0)}_postgres
    environment:
      - POSTGRES_PASSWORD=odoo
      - POSTGRES_USER=odoo
      - PGDATA=/var/lib/postgresql/data/pgdata
    volumes:
      - odoo-db-data:/var/lib/postgresql/data/pgdata

volumes:
  odoo-web-data:
  odoo-db-data:
''';

      composeFile.writeAsStringSync(contents);

      /*---------------------------------------
                  creating odoo instance
      ----------------------------------------*/
      ProcessResult instanceResult = await Process.run('docker-compose', [
        '-f',
        '${buildDir.path}/docker-compose.yaml',
        '-p',
        '${build.elementAt(0)}',
        'up',
        '-d'
      ]);
      if (instanceResult.exitCode != 0) {
        return instanceResult.stderr;
      }
    }

    return result;
  }
}
