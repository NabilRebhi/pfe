import 'dart:io';

class OdooLogs {
  OdooLogs();

  static Future<String> getLogs({required String containerName}) async {
    late String result = "none";
    ProcessResult processResult =
        await Process.run('docker', ['logs', containerName]);
    if (processResult.exitCode != 0) {
      result = "Failed to get instance logs";
    } else {
      result = processResult.stderr;
    }
    return result;
  }
}
