import 'dart:convert';
import 'dart:io';

import 'package:iwaybot/functions/encryption.dart';
import 'package:iwaybot/functions/get_path.dart';

class AdminCredentials {
  AdminCredentials();

  static Future<String> saveAdminCredentials(
      {required String username, required String password}) async {
    late String result = "none";
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    final credentials = {
      "username": Encryption.generateMd5(username),
      "password": Encryption.generateMd5(password)
    };

    try {
      final jsonString = json.encode(credentials);
      await File('${directory.path}/admin.json').writeAsString(jsonString);
      result = "done";
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

  static Future<String> getAdminCredentials(
      {required String username, required String password}) async {
    late String result = "none";
    late dynamic credentials;
    final String dir = GetPath.path.getPath() ?? '';
    final Directory directory = Directory(dir);
    try {
      final jsonString =
          await File('${directory.path}/admin.json').readAsString();
      credentials = json.decode(jsonString);
      if (credentials['username'] == Encryption.generateMd5(username) &&
          credentials['password'] == Encryption.generateMd5(password)) {
        result = "done";
      } else {
        result = "Wrong username or password !";
      }
    } catch (e) {
      result = e.toString();
    }

    return result;
  }
}
