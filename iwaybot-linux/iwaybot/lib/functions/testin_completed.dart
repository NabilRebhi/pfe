import 'dart:io';

class Test {
  Test();
  static Future<String> completed({required List? moduleData}) async {
    final List module = moduleData ?? [];
    late String result = "none";
    if (module == []) {
      result = "ERROR";
    } else {
      ProcessResult processBranchResult = await Process.run('curl', [
        '--request',
        'POST',
        '--header',
        'PRIVATE-TOKEN: ${module.elementAt(2)}',
        'https://gitlab.com/api/v4/projects/${module.elementAt(6)}%2F${module.elementAt(7)}/merge_requests',
        '--data',
        'source_branch=${module.elementAt(1)}&target_branch=staging&title=adding-the-module-to-the-staging-environment'
      ]);
      if (processBranchResult.exitCode != 0) {
        result = "failed to add";
      } else {
        result = "done";
      }
    }
    return result;
  }
}
