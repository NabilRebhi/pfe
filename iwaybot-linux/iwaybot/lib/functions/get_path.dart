import 'package:file_picker/file_picker.dart';
import 'package:iwaybot/data/credentials_path.dart';
import 'package:iwaybot/data/work_directory_path.dart';
import 'package:iwaybot/functions/alerts.dart';

class GetPath {
  GetPath();
  static PathData path = PathData();
  static PathWorkData wDir = PathWorkData();

  static Future<String> startingChoose() async {
    late String result;

    try {
      final getpath = await FilePicker.platform.getDirectoryPath();
      if (getpath != null) {
        path.setPath(getpath);
        result = "done";
      }
    } catch (e) {
      result = e.toString();
    }

    return result;
  }

  static Future<String> workDirectoryChoose(context) async {
    late String result = "none";

    try {
      final getpath = await FilePicker.platform.getDirectoryPath();
      if (getpath != null) {
        wDir.setPath(getpath);
        result == "done";
      }
    } catch (e) {
      Alerts.workingDirectoryEroor(context);
    }

    return result;
  }
}
