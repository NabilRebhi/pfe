import 'dart:async';
import 'package:flutter/material.dart';
import 'package:iwaybot/postgres_utils/postgres_services.dart';

class Modules extends StatefulWidget {
  const Modules({super.key});

  @override
  State<Modules> createState() => _ModulesState();
}

class _ModulesState extends State<Modules> {
  late Timer _refreshTimer;
  late String getModulesResult = "none";
  late List<TableRow> rows = [];
  late int length = -1;

  Future<void> _refresh() async {
    setState(() {
      List list = PostgresServices.modules.getRow() ?? [];
      length = list.length;
    });
  }

  @override
  void initState() {
    super.initState();

    // Set up a timer to refresh the list every 5 seconds
    _refreshTimer = Timer.periodic(const Duration(seconds: 5), (timer) {
      _refresh();
    });
  }

  @override
  void dispose() {
    // Cancel the timer when the widget is disposed
    _refreshTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (length == 0) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Icon(
              Icons.warning_rounded,
              size: 150,
              color: Colors.orange,
            ),
            Text(
              "There are no modules",
              style: TextStyle(
                fontSize: 30,
                color: Colors.orange,
              ),
            ),
            Text(
              "added to the database",
              style: TextStyle(
                fontSize: 30,
                color: Colors.orange,
              ),
            ),
          ],
        ),
      );
    } else if (length > 0) {
      return RefreshIndicator(
        onRefresh: _refresh,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              const Center(
                child: Text(
                  "Modules",
                  style: TextStyle(
                    fontSize: 60,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.1,
              ),
              Table(
                border: TableBorder.all(color: Colors.black),
                children: [
                  const TableRow(
                    children: [
                      DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.black38,
                        ),
                        child: Text(
                          "Module ID",
                          textAlign: TextAlign.center,
                        ),
                      ),
                      DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.black38,
                        ),
                        child: Text(
                          "Branch",
                          textAlign: TextAlign.center,
                        ),
                      ),
                      DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.black38,
                        ),
                        child: Text(
                          "Token",
                          textAlign: TextAlign.center,
                        ),
                      ),
                      DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.black38,
                        ),
                        child: Text(
                          "Odoo Version",
                          textAlign: TextAlign.center,
                        ),
                      ),
                      DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.black38,
                        ),
                        child: Text(
                          "Work directory",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                  for (int index = 0; index < length; index++)
                    (TableRow(
                      children: [
                        Text(
                          PostgresServices.modules
                              .getRow()!
                              .elementAt(index)
                              .elementAt(0)
                              .toString(),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          PostgresServices.modules
                              .getRow()!
                              .elementAt(index)
                              .elementAt(1)
                              .toString(),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          PostgresServices.modules
                              .getRow()!
                              .elementAt(index)
                              .elementAt(2)
                              .toString(),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          PostgresServices.modules
                              .getRow()!
                              .elementAt(index)
                              .elementAt(3)
                              .toString(),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          PostgresServices.modules
                              .getRow()!
                              .elementAt(index)
                              .elementAt(4)
                              .toString(),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    )),
                ],
              ),
            ],
          ),
        ),
      );
    }
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Icon(
            Icons.sync,
            size: 150,
            color: Colors.blue,
          ),
          Text(
            "Loading",
            style: TextStyle(
              fontSize: 50,
              color: Colors.blue,
            ),
          ),
        ],
      ),
    );
  }
}
