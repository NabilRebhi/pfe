import 'dart:io';

import 'package:flutter/material.dart';
import 'package:iwaybot/postgres_utils/postgres_services.dart';
import 'package:iwaybot/views/sign_in.dart';
import 'package:iwaybot/views/starting_page.dart';
import 'package:window_size/window_size.dart';
import 'functions/get_path.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (Platform.isWindows || Platform.isLinux) {
    setWindowTitle('I-way Bot');
    setWindowMaxSize(Size.infinite);
    setWindowMinSize(const Size(800, 600));
  }

  late String result = "none";

  GetPath.path.setPath("/home/${Platform.environment['USER']}");

  await PostgresServices.checkDatabaseCredentialsExistance(
          Directory(GetPath.path.getPath() ?? ''))
      .then((value) {
    result = value;
  });
  if (result == "done") {
    runApp(const MyApp1());
  } else {
    runApp(const MyApp());
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'I-way Bot',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const StartingPage(),
    );
  }
}

class MyApp1 extends StatelessWidget {
  const MyApp1({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'I-way Bot',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SignIn(),
    );
  }
}
